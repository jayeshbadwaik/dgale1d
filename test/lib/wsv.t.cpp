#include <catch2/catch.hpp>
#include <dgale/wsv.hpp>

auto generate_validinput01() -> dgale::wsv_data<std::int64_t>;

auto generate_validinput01() -> dgale::wsv_data<std::int64_t>
{
  auto wsv = dgale::wsv_data<std::int64_t>();
  auto& data = wsv.data;
  std::size_t ndata = 3;
  std::size_t npoint = 7;
  data.resize(ndata);
  for (auto& col : data) {
    col.resize(npoint);
  }

  for (std::size_t i = 0; i < npoint; ++i) {
    for (std::size_t j = 0; j < ndata; ++j) {
      data[j][i] = static_cast<std::int64_t>(10 * i + j + 1);
    }
  }

  return wsv;
}

TEST_CASE("fileio : wsv: read validinput01.wsv", "[fileio][wsv]")
{
  auto const validinput01
    = std::string("test/share/testdata/wsv/test01_validinput01.wsv");
  auto const wsvdata = dgale::read_wsv<std::int64_t>(validinput01);

  auto const exact_data = generate_validinput01();

  REQUIRE(wsvdata == exact_data);
}
