#include <catch2/catch.hpp>
#include <dgale/burger.hpp>

void f(dgale::burger b) {}

TEST_CASE("burger: construct", "[burger]") {
  // Set from File Later
  auto const ic = dgale::burger::initial_condition_t::constant;
  auto const nf = dgale::burger::numerical_flux_t::rusanov;

  auto const burger = dgale::burger(ic, nf);

  auto ic_func = burger.ic_func();
  
  REQUIRE(ic_func(-1.0) == 0);
  REQUIRE(ic_func(0.0) == 0);
  REQUIRE(ic_func(1.0) == 0);
}
