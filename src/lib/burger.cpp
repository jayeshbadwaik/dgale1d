#include <dgale/burger.hpp>

namespace dgale {

burger::burger(initial_condition_t initial_condition,
               numerical_flux_t numerical_flux)
    : initial_condition_(initial_condition), numerical_flux_(numerical_flux) {

  switch (initial_condition_) {
  case initial_condition_t::constant: {
    ic_func_ = [](double x) -> double { return 0.0; };
    break;
  }
  case initial_condition_t::shock: {
    ic_func_ = [](double x) -> double {
      if (x > 0.0) {
        return 0.0;
      } else {
        return 1.0;
      };
    };
    break;
  }
  case initial_condition_t::rarefaction: {
    ic_func_ = [](double x) -> double {
      if (x > 0.0) {
        return 1.0;
      } else {
        return 0.0;
      };
    };
    break;
  }
  default:
    throw std::runtime_error("Unexpected Initial Condition");
  }
}

auto burger::ic_func() const -> std::function<double(double)> {
  return ic_func_;
}

} // namespace dgale
