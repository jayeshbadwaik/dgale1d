#ifndef BURGER_HPP
#define BURGER_HPP

#include <functional>

namespace dgale {
class burger {
public:
  enum class initial_condition_t { constant = 0, shock = 1, rarefaction = 2 };
  enum class numerical_flux_t { rusanov = 0 };

public:
  burger() = default;

  explicit burger(initial_condition_t ic, numerical_flux_t numerical_flux);

  auto ic_func() const -> std::function<double(double)>;

  auto exact_exists() const -> bool;

  // If exact_exists() returns false, then behavior of the function
  // exact_solution() is undefined
  auto exact_solution(double t) -> std::function<double(double)>;

  auto flux(double u) -> double;

  auto numerical_flux(double ul, double ur) -> double;

  // TODO : uncomment this later and fill the definition
  // auto characteristic_flux(double u, double gradient) -> double;

private:
  initial_condition_t initial_condition_;
  numerical_flux_t numerical_flux_;
 
  std::function<double(double)> ic_func_;
};
} // namespace dgale

#endif // BURGER_HPP
