#ifndef DGALE_WSV_HPP
#define DGALE_WSV_HPP

#include <fstream>
#include <sstream>
#include <vector>

namespace dgale {

template <typename T>
struct wsv_data {
  wsv_data() = default;

  std::vector<std::vector<T>> data;
};

template <typename T>
auto read_wsv(std::string const& filename) -> wsv_data<T>;

template <typename T>
void write_wsv(wsv_data<T> const& data, std::string const& filename);

template <typename T>
auto read_wsv(std::string const& filename) -> wsv_data<T>
{
  auto wsv = wsv_data<T>();

  auto& data = wsv.data;

  unsigned int ndata = 0;

  auto in = std::ifstream(filename);

  std::string input_string;

  std::getline(in, input_string);

  std::istringstream first_line(input_string);
  while (first_line >> input_string) {
    ++ndata;
  }

  data.resize(ndata);

  in.seekg(0, std::ios::beg);

  while (!in.eof()) {
    for (unsigned int i = 0; i < ndata; ++i) {
      T input_data;
      in >> input_data;
      if (in.eof()) {
        break;
      }
      data[i].push_back(input_data);
    }
  }
  return wsv;
}

template <typename T>
bool operator==(wsv_data<T> const& a, wsv_data<T> const& b)
{
  auto constexpr tolerance = std::numeric_limits<T>::epsilon();

  if (a.data.size() != b.data.size()) {
    return false;
  }

  for (std::size_t i = 0; i < a.data.size(); ++i) {
    if (a.data[i].size() != b.data[i].size()) {
      return false;
    }
  }

  for (std::size_t i = 0; i < a.data.size(); ++i) {
    for (std::size_t j = 0; j < b.data.size(); ++j) {
      if (std::abs(a.data[i][j] - b.data[i][j]) > tolerance) {
        return false;
      }
    }
  }

  return true;
}

} // namespace dgale

#endif // DGALE_WSV_HPP
